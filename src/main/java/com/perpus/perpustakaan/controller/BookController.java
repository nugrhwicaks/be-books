package com.perpus.perpustakaan.controller;

import com.perpus.perpustakaan.entities.Books;
import com.perpus.perpustakaan.services.BookServices;
import com.perpus.perpustakaan.services.BookServicesImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(value="/api")
public class BookController {
    @Autowired
    private BookServices bookServices;
    @Autowired
    private BookServicesImpl bookServicesImpl;

    @GetMapping(value="/AllBook")
    public ResponseEntity<List<Books>> showBooks(){
        List<Books> tampil= bookServicesImpl.getAll();
        return new ResponseEntity<>(tampil, HttpStatus.OK);
    }
    @PostMapping(value="/AddData")
    public ResponseEntity<List<Books>> adddata(@RequestParam(name="id")int idbook,
                                               @RequestParam(name="namabook")String namebook,
                                               @RequestParam(name="author")String author,
                                               @RequestParam(name="year")LocalDate year){
        return new ResponseEntity<>(bookServices.addBook(idbook, namebook, author, year),HttpStatus.OK);
    }
    @PutMapping(value="/editdata/{id}")
    public ResponseEntity<Books> editdata(@PathVariable(name="id")int idbook,
                                          @RequestParam(name="namebook")String namebook,
                                          @RequestParam(name="author")String author,
                                          @RequestParam(name="year")LocalDate year){

        Books editdata=bookServices.editBook(idbook,namebook,author, year);
        return new ResponseEntity<>(editdata, HttpStatus.OK);
    }

}
