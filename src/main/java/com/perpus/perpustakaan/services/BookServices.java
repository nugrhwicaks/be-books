package com.perpus.perpustakaan.services;

import com.perpus.perpustakaan.entities.Books;

import java.time.LocalDate;
import java.util.List;

public interface BookServices {
    public abstract List<Books> getAllBook();

    public abstract List<Books> addBook(int idbook, String nameBook, String author, LocalDate year);

    public abstract Books editBook(int idbook, String nameBook, String author,LocalDate year);



}
