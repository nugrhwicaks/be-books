package com.perpus.perpustakaan.services;

import com.perpus.perpustakaan.entities.Books;
import com.perpus.perpustakaan.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.stream.IntStream;
@Service
public class BookServicesImpl implements BookServices {
    @Autowired
    private BookRepository bookRepo;
    @Override
    public List<Books> getAllBook(){
        bookRepo.findAll();
        return null;
    }
    public List<Books> getAll(){
        List<Books> listbooks=List.of(
                    new Books(323,"BUku Mantra","Harry", LocalDate.of(190,Month.AUGUST,23)),
                    new Books(354,"BUku sapu","Budi", LocalDate.of(2000,Month.APRIL,5)),
                    new Books(423,"Buku matematika", "Putra", LocalDate.of(2019,Month.DECEMBER,2)),
                    new Books(546,"Buku Resep ","Ibu ana",LocalDate.of(2023,Month.JANUARY,3)),
                    new Books(234,"BUku Rumus", "pak ahon",LocalDate.of(2020,Month.JULY,9)))  ;
        return listbooks;
    }
    @Override
    public List<Books> addBook(int idbook, String nameBook, String author, LocalDate year) {
        List<Books> addlist=List.of(new Books(idbook,nameBook,author,year));
        return addlist;
    }
    @Override
    public Books editBook(int idbook,String nameBook,String author, LocalDate year){
        List<Books> data = getAll();
        int index = IntStream.range(0, data.size())
                .filter(i -> data.get(i).getIdbook()== idbook)
                .findFirst()
                .orElse(0);

        Books editdata=data.get(index);
        editdata.setBookName(nameBook);
        editdata.setAuthor(author);
        editdata.setYear(year);
        editdata.setIdbook(idbook);

        System.out.println(editdata.setIdbook(idbook));
        System.out.println(index);
        System.out.println(editdata.setBookName(nameBook));
        return editdata;

    }




}
